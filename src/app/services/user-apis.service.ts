import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable, of } from 'rxjs';
import { map} from 'rxjs/operators';
import {environment} from 'environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UserApisService {
   endpoint =  environment.apiUrl; //"https://jsonplaceholder.typicode.com";
  constructor(private http: HttpClient) { }


  fetchUsers(): Observable<any>{
    console.log("fetching users");
   return this.http.get(this.endpoint + "/users");
  }

  getUsersAlbums(): Observable<any>{
    return this.http.get(this.endpoint + "/albums");
  }

  getPhotos():Observable<any>{
    return this.http.get(this.endpoint + "/photos");
  }
}
