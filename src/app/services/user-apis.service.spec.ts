import { TestBed, inject } from '@angular/core/testing';

import { UserApisService } from './user-apis.service';

describe('UserApisService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserApisService]
    });
  });

  it('should be created', inject([UserApisService], (service: UserApisService) => {
    expect(service).toBeTruthy();
  }));
});
