import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {UserApisService} from '../services/user-apis.service';
@Component({
  selector: 'app-user-photos',
  templateUrl: './user-photos.component.html',
  styleUrls: ['./user-photos.component.css']
})
export class UserPhotosComponent implements OnInit {
  photos: any;
  @Input() showPhotos: any;
  @Input() albumId: any;
  @Output() hidePhotos = new EventEmitter();
  constructor(private getPhotos: UserApisService) { }

  ngOnInit() {
    this.getAlbumPhotos();
  }

  getAlbumPhotos(){
    this.getPhotos.getPhotos().subscribe(res=>{
      this.photos = res;
    })
  }

  filterPhotos(){
    return this.photos.filter(photo => photo.albumId === this.albumId).slice(0,5);
  }

  backToAlbums(){
    this.hidePhotos.emit();
  }

}
