import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {UserApisService} from '../services/user-apis.service';
@Component({
  selector: 'app-user-albums',
  templateUrl: './user-albums.component.html',
  styleUrls: ['./user-albums.component.css']
})
export class UserAlbumsComponent implements OnInit {
  albumList: any;
  @Input() userId: any; 
  @Output() hideAlbums = new EventEmitter();
  albumId: any;
  hideAlbumList: boolean;
  showAlbumPhotos: boolean;
  constructor(private getAlbums: UserApisService) { }

  ngOnInit() {
    this.getUserAlbums();
  }
  getUserAlbums(){
    this.getAlbums.getUsersAlbums().subscribe(res =>{
      this.albumList = res;
    }
    );
  }

  filterAlbums(){
    return this.albumList.filter(albums => albums.userId === this.userId);
  }

  showPhotos(albumId:any){
    this.albumId =albumId;
    this.hideAlbumList = true;
    this.showAlbumPhotos = true;
  }

  hideAlbumPhotos(){
    this.hideAlbumList = false;
    this.showAlbumPhotos = false;
  }

  backToUserList(){
    this.hideAlbums.emit();
  }
}
