import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UserAlbumsComponent } from './user-albums/user-albums.component';
import { UserPhotosComponent } from './user-photos/user-photos.component';
import {UserApisService} from 'app/services/user-apis.service';
@NgModule({
  declarations: [
    AppComponent,
    UsersListComponent,
    UserAlbumsComponent,
    UserPhotosComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [UserApisService],
  bootstrap: [AppComponent]
})
export class AppModule { }
