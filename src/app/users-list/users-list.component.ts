import { Component, OnInit } from '@angular/core';
 import {UserApisService} from '../services/user-apis.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css'],
})

export class UsersListComponent implements OnInit {
  usersList: any;
  userId: number
  hideUserList: boolean;
  showAlbumList: boolean;
  constructor(private getUsers: UserApisService) { }

  ngOnInit() {
   this.getAllUsers();
  }

  getAllUsers(){
    this.getUsers.fetchUsers().subscribe(res=>{
      this.usersList = res;
      console.log(this.usersList);
    });
  }

  showUserAlbums(userId: any){
    this.userId = userId;
    this.hideUserList = true;
    this.showAlbumList = true;
  }

  hideUserAlbums(){
    this.hideUserList = false;
    this.showAlbumList = false;
  }
}
